package com.example.extraextraapp

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.extraextraapp.model.response.Article
import com.example.extraextraapp.state.AppleNewsState
import com.example.extraextraapp.ui.theme.ExtraExtraAppTheme
import com.example.extraextraapp.utils.API_KEY
import com.example.extraextraapp.utils.EVERYTHING_PATH
import com.example.extraextraapp.viewModel.AppleNewsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    private val appleNewsViewModel by viewModels<AppleNewsViewModel>()

    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val appleNewsState: AppleNewsState by appleNewsViewModel.appleNews.collectAsState()
            appleNewsViewModel.getAppleNews(q = EVERYTHING_PATH, api = API_KEY)
            ExtraExtraAppTheme {
                Scaffold(
                    topBar = {
                        TopAppBar(
                            title = { Text("Extra! Extra!") },
                            backgroundColor = MaterialTheme.colors.primary,
                        )
                    }
                ) {
                    Surface(modifier = Modifier.fillMaxSize()) {
                        SearchResult(
                            appleNewsState = appleNewsState
                        )
                    }
                }
            }
        }
    }

    @Composable
    fun SearchResult(
        appleNewsState: AppleNewsState
    ) {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colors.background)
        ) {
            items(appleNewsState.appleResponse.articles) { appleNews: Article ->
                Card(
                    elevation = 4.dp,
                    shape = MaterialTheme.shapes.medium,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp),
                    backgroundColor = MaterialTheme.colors.primary
                ) {
                    Column(modifier = Modifier.padding(16.dp)) {
                        Text(
                            text = appleNews.title,
                            modifier = Modifier.fillMaxSize(),
                            color = MaterialTheme.colors.onPrimary,
                            fontSize = 16.sp
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = appleNews.description,
                            color = MaterialTheme.colors.onPrimary,
                            fontSize = 14.sp
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = appleNews.content,
                            color = MaterialTheme.colors.onPrimary,
                            fontSize = 14.sp
                        )
                    }
                }
            }
        }
    }
}

