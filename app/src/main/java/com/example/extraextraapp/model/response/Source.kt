package com.example.extraextraapp.model.response


import com.google.gson.annotations.SerializedName

data class Source(
    val id: String? = "",
    val name: String = ""
)