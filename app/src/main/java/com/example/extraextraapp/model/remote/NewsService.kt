package com.example.extraextraapp.model.remote

import com.example.extraextraapp.model.response.NewsResponse
import com.example.extraextraapp.utils.API_KEY
import com.example.extraextraapp.utils.APPLE_QUERY
import com.example.extraextraapp.utils.EVERYTHING_PATH
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NewsService {

    @GET(EVERYTHING_PATH)
    suspend fun getAppleNews(
        @Path("everything")
        @Query(APPLE_QUERY) q: String,
        @Query(API_KEY) api: String,
    ): NewsResponse
}