package com.example.extraextraapp.model.domain

import com.example.extraextraapp.model.remote.NewsRepo
import com.example.extraextraapp.model.response.Article
import com.example.extraextraapp.model.response.NewsResponse
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetAppleNewsUseCase @Inject constructor(private val newsRepo: NewsRepo) {
    suspend operator fun invoke(q: String, api: String): Result<NewsResponse> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val appleResponse = newsRepo.getAppleNews(q, api)
                Result.success(appleResponse)
            } catch (ex: Exception) {
                Result.failure(ex)
            }
        }
}