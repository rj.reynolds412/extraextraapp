package com.example.extraextraapp.di

import com.example.extraextraapp.model.domain.GetAppleNewsUseCase
import com.example.extraextraapp.model.remote.NewsRepo
import com.example.extraextraapp.model.remote.NewsService
import com.example.extraextraapp.utils.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NewsModule {

    @Provides
    @Singleton
    fun providesNewsService(): NewsService = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(NewsService::class.java)

    @Provides
    @Singleton
    fun providesNewsRepo(newsService: NewsService): NewsRepo = NewsRepo(newsService)

    @Provides
    @Singleton
    fun providesAppleNewsUseCase(newsRepo: NewsRepo): GetAppleNewsUseCase = GetAppleNewsUseCase(newsRepo)
}
