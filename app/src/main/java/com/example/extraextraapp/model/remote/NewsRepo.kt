package com.example.extraextraapp.model.remote

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NewsRepo @Inject constructor(private val newsService: NewsService) {
    suspend fun getAppleNews(q: String, api: String) = withContext(Dispatchers.IO) {
        newsService.getAppleNews(q, api).also { Log.d("NewsRepo", "getAppleNews: ${newsService.getAppleNews(q, api)} ") }
    }
}