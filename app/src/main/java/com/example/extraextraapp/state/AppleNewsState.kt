package com.example.extraextraapp.state

import com.example.extraextraapp.model.response.Article
import com.example.extraextraapp.model.response.NewsResponse

data class AppleNewsState(
    val isLoading: Boolean = false,
    val appleResponse: NewsResponse = NewsResponse(),
    val error: Throwable? = null
)