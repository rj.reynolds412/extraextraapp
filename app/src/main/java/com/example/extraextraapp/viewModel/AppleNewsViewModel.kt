package com.example.extraextraapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.extraextraapp.model.domain.GetAppleNewsUseCase
import com.example.extraextraapp.model.response.Article
import com.example.extraextraapp.model.response.NewsResponse
import com.example.extraextraapp.state.AppleNewsState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

@HiltViewModel
class AppleNewsViewModel @Inject constructor(private val getAppleNewsUseCase: GetAppleNewsUseCase) :
    ViewModel() {

    private val _appleNews = MutableStateFlow(AppleNewsState())
    val appleNews get() = _appleNews.asStateFlow()

    fun getAppleNews(q: String, api: String ) {
        viewModelScope.launch(Dispatchers.IO) {
            _appleNews.value = _appleNews.value.copy(isLoading = true)
            val appleNewsResult = getAppleNewsUseCase.invoke(
                q = q,
                api = api
            )
            if (appleNewsResult.isSuccess) {
                val appleNewsSearch: NewsResponse = appleNewsResult.getOrThrow()
                _appleNews.value = _appleNews.value.copy(
                    appleResponse = appleNewsSearch
                )
            } else {
                _appleNews.value = _appleNews.value.copy(
                    error = appleNewsResult.exceptionOrNull()
                )
            }
            _appleNews.value = _appleNews.value.copy(
                isLoading = false
            )
        }
    }
}